require('dotenv').config();
import { Selector } from 'testcafe';
import { writeFileSync } from 'fs';
const path = require('path');
var _ = require('lodash');

function script(application) {
  return _.mapValues(application.tablesById, table =>
    _.set(
      _.omit(table, ['sampleRows']),
      'columns',
      _.map(table.columns, item =>
        _.set(item, 'foreignTable', _.get(item, 'foreignTable.id'))
      )
    )
  );
}

function getUrl(baseId) {
  return `https://airtable.com/${baseId}/api/docs`;
}
function getSavePath(baseId) {
  return path.resolve(__dirname, 'downloads', `${baseId}.json`);
}

var baseId = process.env.AIRTABLE_BASE_KEY;
var email = process.env.USER_EMAIL;
var password = process.env.USER_PWD;

fixture`Get JSON Schema for table`.page(getUrl(baseId));

test('Run test', async t => {
  await t.wait(1000);
  await t.expect('#sign-in-form-fields-root > div > label > div').ok();

  await t
    .typeText(
      '#sign-in-form-fields-root > div > label > input[name="email"]',
      email
    )
    .typeText(
      '#sign-in-form-fields-root > div > label > input[name="password"]',
      password
    )
    .click('#sign-in-form-fields-root > div > label > input[type="submit"]')
    .wait(1000);
  await t.expect('.docs > .languageTabs > .tab').ok();

  const application = await t.eval(() => window.application);
  const json = JSON.stringify(script(application), function(key, value) {
    if (key === '$$hashKey') {
      return undefined;
    }

    return value;
  });
  writeFileSync(getSavePath(baseId), json);
});
